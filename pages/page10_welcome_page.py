from page_objects import PageObject, PageElement


class WelcomePage(PageObject):

    login_link = PageElement(link_text="Login")


    def check_page(self):
        return "Welcome" in self.w.title


    def click_login(self, loginPage):

        self.login_link.click()

        return loginPage.check_page()

